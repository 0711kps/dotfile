prepareTools(){
  if command -v aptitude 2>&1 >/dev/null && command -v git 2>&1 >/dev/null && command -v curl 2>&1 >/dev/null
  then
    completeText 'git curl and aptitude already installed!'
    echo ''
  else
    noticeText 'start to install git curl and aptitude...'
    echo ''
    sudo apt install git aptitude curl -y
    completeText 'git curl and aptitude installation complete!'
    echo ''
  fi
  if command -v brew 2>&1 >/dev/null
  then
    completeText 'brew already installed!'
    echo ''
  else
    noticeText 'start to install linuxbrew...'
    echo ''
    bash -c "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install.sh)"
    eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)
    completeText 'linuxbrew installation complete!'
    echo ''
  fi
}
