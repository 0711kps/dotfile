aptApps(){
  noticeText 'start to install app from ubuntu repo...'
  echo ''
  aptBasicDep=(build-essential cmake make gcc clang)

  aptCli+=(at curl git tree xclip neovim htop p7zip zsh git-flow mldonkey-server nano)
  aptDatabase+=(mongodb mysql-server redis postgresql)
  aptFonts+=(fonts-ricty-diminished fonts-noto-cjk)
  aptHttpServer+=(nginx apache2)
  aptInputMethod+=(fcitx fcitx-rime fcitx-mozc librime-data-double-pinyin librime-data-combo-pinyin)
  aptLooks+=(sugar-themes arc-theme gnome-tweaks)
  aptMediaCreate+=(gimp)
  aptXTool+=(vlc firefox gnome-tweaks deluge mldonkey-gui)
  if [ "$ENV" = 'full' ]
  then
    aptCli+=(ffmpeg)
    aptInternet+=(openssh-server)
    aptXTool+=(simplescreenrecorder)
  fi

  aptList=(${aptBasicDep[@]} ${aptCli[@]} ${aptDatabase[@]} ${aptFonts[@]} ${aptInputMethod[@]} ${aptInternet[@]} ${aptLooks[@]} ${aptMediaCreate} ${aptXTool[@]})
  unset aptBasicDep aptCli aptDatabase aptFonts aptInputMethod aptInternet aptLooks aptMediaCreate aptXTool
  sudo print # trigger sudo
  aptProcess &
  que+=($!)
}

aptProcess(){
  sudo aptitude update 1>/dev/null 2>>log/apt.log
  sudo aptitude install -y ${aptList[@]} 1>/dev/null 2>>log/apt.log # 1>??? can skip 1 and written as >???
  if [ $? -eq 0 ]
  then
    completeText 'apt install complete!'
    rm -f log/apt.log
  else
    failText 'apt install fail!'
  fi
  echo ''
  unset aptList
}

brewApps(){
  noticeText 'start to install app from brew...'
  echo ''
  brewLang+=(ruby nodejs yarn)
  if [ "$ENV" = 'full' ]
  then
    brewLargeCli+=(docker)
  fi
  brewList+=(${brewLang[@]} ${brewLargeCli[@]})
  unset brewLang brewLargeCli
  brewProcess &
  que+=($!)
}

brewProcess(){
  brew install ${brewList[@]} 1>/dev/null 2>>log/brew.log
  if [ $? -eq 0 ]
  then
    completeText 'brew install complete!'
    rm -f log/brew.log
  else
    failText 'brew install fail!'
  fi
  unset brewList
  echo ''
}

snapApps(){
  noticeText 'start to install app from snap...'
  echo ''
  snapChartCreate+=(freeplane-mindmapping)
  if [ "$ENV" = 'full' ]
  then
    snapImageCreate+=(inkscape)
    snapInternet=(spotify)
  fi
  snapList=(${snapChartCreate[@]} ${snapEditor[@]} ${snapImageCreate[@]} ${snapInternet[@]})
  unset snapChartCreate snapEditor snapImageCreate snapInternet
  sudo print # trigger sudo
  snapProcess &
  que+=($!)
}

snapProcess(){
  sudo snap install ${snapList[@]} 1>/dev/null 2>log/snap.log
  if [ $? -eq 0 ]
  then
    completeText 'snap install complete!'
    rm -f log/snap.log
  else
    failText 'snap install fail'
  fi
  echo ''
  unset snapList
}

otherApps(){
  noticeText 'start to install app from other...'
  echo ''
  # and houdini(https://www.sidefx.com/download/daily-builds)
  (installFcitxDesktop && installRemarkable && installRipgrep && installFD && installBat) 1>/dev/null 2>>log/otherApps.log &
  que+=($!)
  if [ "$ENV" = 'full' ]
  then
    installTMSU 1>/dev/null 2>>log/tmsu.log &
    que+=($!)
  fi
  echo ''
}

installBat(){
  wget "https://github.com/sharkdp/bat/releases/download/v0.10.0/bat_0.10.0_amd64.deb" &&
  sudo dpkg -i bat_0.10.0_amd64.deb &&
  rm -f bat_0.10.0_amd64.deb
}

installRemarkable(){
  aria2c "http://remarkableapp.github.io/files/remarkable_1.87_all.deb" &&
  sudo dpkg -i remarkable_1.87_all.deb &&
  rm -f remarkable_1.87_all.deb
}

installRipgrep(){
  wget "https://github.com/BurntSushi/ripgrep/releases/download/11.0.1/ripgrep_11.0.1_amd64.deb" &&
  sudo dpkg -i ripgrep_11.0.1_amd64.deb &&
  rm -f ripgrep_11.0.1_amd64.deb
}

installFD(){
  wget "https://github.com/sharkdp/fd/releases/download/v7.3.0/fd_7.3.0_amd64.deb" &&
  sudo dpkg -i fd_7.3.0_amd64.deb &&
  rm -f fd_7.3.0_amd64.deb
}

removeApps(){
  noticeText "removing unnecessary apps..."
  echo ''
  sudo print # trigger sudo
  yes | sudo aptitude remove -y ubuntu-software thunderbird pidgin gedit 1>/dev/null 2>log/remove.log &
  que+=($!)
  echo ''
}

installTMSU(){
  aria2c "https://github.com/oniony/TMSU/releases/download/v0.7.4/tmsu-x86_64-0.7.4.tgz" &&
  7z x -aoa tmsu-x86_64-0.7.4.tgz &&
  7z x -aoa tmsu-x86_64-0.7.4.tar &&
  sudo mv tmsu-x86_64-0.7.4 /opt/tmsu &&
  sudo chown -R $USER:$USER /opt/tmsu &&
  sudo chmod 755 /opt/tmsu /opt/tmsu/*/ /opt/tmsu/bin/* &&
  rm -f tmsu*
}

# installYandex - go download deb at official site

installFcitxDesktop(){
  aria2c "https://download.fcitx-im.org/fcitx/fcitx-4.2.9.tar.xz" &&
  7z x -aoa fcitx-4.2.9.tar.xz &&
  7z x -aoa fcitx-4.2.9.tar &&
  cp fcitx-4.2.9/data/fcitx-autostart.desktop.in /etc/xdg/autostart/fcitx.desktop &&
  rm -rf fcitx-4.2.9* pax_global_header
}

nodeApps(){
  /home/linuxbrew/.linuxbrew/bin/yarn global add --force jshint stylelint jsonlint pug-lint yaml-lint vint shellcheck 1>/dev/null 2>>log/node.log &
  que+=($!)
}

gemApps(){
  /home/linuxbrew/.linuxbrew/bin/gem --force install pry-byebug rubocop rake bundler slim_lint 1>/dev/null 2>>log/gem.log &
  que+=($!)
}

#### configure app ####

afterInstall(){
  configureGit
  configureVim
  configureFcitx
  configureHardware
  configureSSH
  configureZsh
  if [ "$env" == 'full' ]
  then
    configureRsync
    #configureEmacs
  fi
}

configureGit(){
  git config --global user.email "0711kps@gmail.com"
  git config --global user.name "Azuma Shibada"
  git config --global core.editor "nvim"
}

configureVim(){
  mkdir -p ~/.config/nvim/autoload &&
  cp -f configs/vim/plug.vim ~/.config/nvim/autoload &&
  cp -f configs/vim/init.vim ~/.config/nvim/init.vim &&
  cp -f configs/vim/keybind.vim ~/.config/nvim/keybind.vim &&
  cp -rf configs/vim/snippets ~/.config/nvim/
}

configureFcitx(){
  im-config -n fcitx &&
  mkdir -p ~/.config/fcitx/rime &&
  cp -f configs/fcitx/default.custom.yaml ~/.config/fcitx/rime/default.custom.yaml
}

configureHardware(){
  sudo mkdir -p /usr/share/X11/xorg.conf.d &&
  sudo cp -f configs/hardware/10-libinput.conf /usr/share/X11/xorg.conf.d/
}

configureSSH(){
  mkdir -p ~/.ssh &&
  ssh-keygen -t rsa -b 4096 -P '' -f ~/.ssh/mykey &&
  echo 'eval "$(ssh-agent -s)"' >> ~/.zshrc &&
  echo 'ssh-add ~/.ssh/mykey' >> ~/.zshrc
}
configureRsync(){
  sudo cp -f configs/rsync/rsyncd.conf /etc/
}
configureZsh(){
  /home/linuxbrew/.linuxbrew/bin/zsh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" &&
  zshrcContent=$(cat configs/zsh/zshrc) &&
  cp -f configs/zsh/alias.zsh ~/.alias.zsh &&
  cp -f configs/zsh/function.zsh ~/.function.zsh &&
  touch ~/.local.zsh &&
  {
    if [ $ENV = 'full' ]; then
      echo 'export PATH="/opt/tmsu:$PATH"'
      echo 'export PATH="/opt/crystal:$PATH"'
    fi

    echo 'source ~/.alias.zsh'
    echo 'source ~/.local.zsh'
    echo 'source ~/.function.zsh'
    echo 'export PATH="~/.yarn/bin:$PATH"'
    echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"'
    echo "$zshrcContent"
  } > ~/.zshrc
  sudo sed -i s/required\ \ \ pam_shells.so/sufficient\ \ \ pam_shells.so/ /etc/pam.d/chsh &&
  sudo chsh $USER -s /usr/bin/zsh &&
  sudo chsh root -s /usr/bin/zsh &&
  sudo sed -i s/sufficient\ \ \ pam_shells.so/required\ \ \ pam_shells.so/ /etc/pam.d/chsh &&
  git clone https://github.com/zsh-users/zsh-autosuggestions ~/.oh-my-zsh/custom/plugins/zsh-autosuggestions &
  que+=($!)
  git clone https://github.com/zsh-users/zsh-completions ~/.oh-my-zsh/custom/plugins/zsh-completions &
  que+=($!)
  git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting &
  que+=($!)
  git clone https://github.com/bobthecow/git-flow-completion ~/.oh-my-zsh/custom/plugins/git-flow-completion
  que+=($!)
}
afterHint(){
  completeText 'process complete! please restart session for some apps to work normally'
  echo ''
}
