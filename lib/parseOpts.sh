parseOpts(){
  while test $# -gt 0; do
    case "$1" in
      -h|--help)
        printf "%s\tshow this help" "$(noticeText '-h | --help')"
        echo ''
        printf "%s\tassign the env variable to use (%s, %s)" "$(noticeText '-e | --env')" "$(hintText 'full')" "$(hintText 'light')"
        echo ''
        exit 0
        ;;
      -e|--env)
        shift
        ENV=$1
        shift
        ;;
      *)
        noticeText "unknown option $(failText \"$1\")"
        echo ''
        while test $# -gt 0; do
	  shift
	done
	exit 1
        ;;
    esac
  done
}
