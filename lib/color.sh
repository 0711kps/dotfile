hintText(){
  printf "\e[1;33m%s\e[0m" "$1"
}

completeText(){
  printf "\e[1;32m%s\e[0m" "$1"
}

failText(){
  printf "\e[1;31m%s\e[0m" "$1"
}
noticeText(){
  printf "\e[1m%s\e[0m" "$1"
}
