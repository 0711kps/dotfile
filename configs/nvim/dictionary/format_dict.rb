dicts = ARGV.filter { |arg| File.exist?(arg) }

def read_words(dict)
  File.read(dict).split("\n").uniq.sort.filter { |word| word.length > 2 }
end

def write_words(dict, words)
  File.open(dict, 'w') do |file|
    words.each { |word| file.write(word + "\n") }
    file.close
  end
end

dict_do = lambda do |dict|
  write_words(dict, read_words(dict))
end

dicts.each(&dict_do)
