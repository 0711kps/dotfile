se number
se noundofile
se noswapfile
syntax on
filetype plugin indent on
filetype plugin on
filetype on
colorscheme gruvbox
se omnifunc=syntaxcomplete#Complete
se expandtab
se tabstop=2
se shiftwidth=2
se incsearch
se mouse=
se encoding=UTF-8
se autoindent
se cmdheight=2
se scrolloff=3
se showcmd
se laststatus=2
se backspace=2
se shortmess=I
se whichwrap+=h,l
se timeoutlen=1000 ttimeoutlen=0
se cursorline
hi CursorLine ctermbg=none cterm=bold
au FileType * execute 'setlocal dict+=~/.config/nvim/dictionary/'.&filetype.'.dict'
