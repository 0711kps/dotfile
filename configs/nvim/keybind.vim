nm <C-x><C-a> gT
nm <C-x><C-w> gTgT
nm <C-x><C-d> gt
nm <C-x><C-x> gtgt

" save and close
nm <C-q><C-q><C-q> :q<CR>
nm <C-q><C-q><C-a> :qa<CR>
nm <C-q><C-w><C-w> :w<CR>
nm <C-q><C-w><C-a> :wa<CR>
nm <C-q><C-x><C-x> :x<CR>
nm <C-q><C-x><C-a> :xa<CR>

vm <C-x><C-y> "+y


" emacs style move
nm <C-p> <Up>
im <C-p> <Up>
vm <C-p> <Up>

nm <C-n> <Down>
im <C-n> <Down>
vm <C-n> <Down>

nm <C-b> <Left>
im <C-b> <Left>
vm <C-b> <Left>

nm <C-f> <Right>
im <C-f> <Right>
vm <C-f> <Right>

nm <C-a> <Home>
im <C-a> <Home>
vm <C-a> <Home>

nm <C-e> <End>
im <C-e> <End>
vm <C-e> <End>
"

nm <silent> <C-w><C-f> :wincmd l<CR>
nm <silent> <C-w><C-b> :wincmd h<CR>
nm <silent> <C-w><C-n> :wincmd j<CR>
nm <silent> <C-w><C-p> :wincmd k<CR>

nm <C-t> :tabnew<SPACE>
im <C-d> <Delete>

" NERDTree
nm <silent> <C-x><C-b> :NERDTreeToggle<CR>
" NERDTree t => open in new tab
" NERDTree T => open in new background tab

" snipMate
im <C-k> <Plug>snipMateNextOrTrigger

" NERDCommenter
nm <C-x>' <plug>NERDCommenterToggle
vm <C-x>' <plug>NERDCommenterToggle

" FZF
nmap <silent> <C-x><C-f> :FZF<CR>

" multi edit
nmap <C-x><C-n> cgn

" ripgrep
nmap <C-x><C-r> :Rg<space>

" easymotion
nm <C-x><C-j> <Plug>(easymotion-s)

" emmet
let g:user_emmet_leader_key = '<C-x>'
