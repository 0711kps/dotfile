let g:ale_completion_enabled = 1
let g:ale_sign_column_always = 1
let g:ale_lint_on_text_changed = 'never'
let g:ale_lint_on_enter = 0
let g:ale_sign_error = '!'
let g:ale_sign_warning = '?'
let g:airline#extensions#ale#enabled = 1

let g:ale_linters = {
\   'javascript': ['jshint'],
\   'ruby': ['rubocop'],
\   'c': ['clang'],
\   'cpp': ['clang'],
\   'scss': ['stylelint'],
\   'sass': ['stylelint'],
\   'css': ['stylelint'],
\   'html': ['stylelint'],
\   'json': ['jsonlint'],
\   'slim': ['slimlint'],
\   'pug': ['puglint'],
\   'yaml': ['yamllint'],
\   'vim': ['vint'],
\   'sh': ['shellcheck']
\}

let g:ale_list_window_size = 3
let g:ale_open_list = 1
let g:ale_keep_list_window_open = 1
