
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(global-unset-key (kbd "C-w"))
(global-unset-key (kbd "C-c C-d"))
(global-unset-key (kbd "C-c C-h"))
(global-set-key (kbd "C-w C-v") 'split-window-right)
(global-set-key (kbd "C-w C-s") 'split-window-below)
(global-set-key (kbd "C-c C-h") 'backward-kill-word)
(global-set-key (kbd "C-c C-d") 'kill-word)
(global-unset-key (kbd "C-h"))
(global-set-key (kbd "C-h") 'delete-backward-char)
(global-set-key (kbd "C-c C-j") 'ace-jump-char-mode)
(global-unset-key (kbd "C-x C-b"))
(global-set-key (kbd "C-c C-<left>") 'undo)
(global-set-key (kbd "C-c C-<right>") 'redo)
(global-set-key (kbd "C-x C-b") 'neotree-toggle)
(global-set-key (kbd "C-x C-j") 'nerdtab-jump)
(global-set-key (kbd "C-c C-f") 'fzf)

(when (version<= "26.0.50" emacs-version)
  (global-display-line-numbers-mode))
(global-hl-line-mode)

(setq-default make-backup-files nil)
(setq-default auto-save-default nil)
(setq-default nerdtab-window-position 'bottom)
(setq inhibit-startup-screen t)

(defun my-startup-fun()
  (load-theme 'badger)
  (global-company-mode)
  (electric-pair-mode)
  (nerdtab-mode))

(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("e7b49145d311e86da34a32a7e1f73497fa365110a813d2ecd8105eaa551969da" default))
 '(package-selected-packages
   '(fzf evil-nerd-commenter neotree nerdtab badger-theme ace-jump-mode yasnippet dart-mode rainbow-delimiters slim-mode company company-shell company-web emmet-mode php-mode rspec-mode scss-mode web-mode)))
(add-hook 'after-init-hook 'my-startup-fun)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(hl-line ((t (:weight ultra-bold))))
 '(line-number-current-line ((t (:weight bold)))))
