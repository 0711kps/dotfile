nvf(){
  fzfFile=$(fd . -t f -E "*.{avi,wmv,mkv,mp4,jpg,bmp,png,mp3,jar}" -d 4 $1 | fzf)
  if [ $fzfFile ]
  then
    nv "$fzfFile"
  fi
}
cf(){
  fzfLocation=$(fd . -t d -d 4 $1 | fzf)
  if [ $fzfLocation ]
  then
    cd "$fzfLocation"
  fi
}
