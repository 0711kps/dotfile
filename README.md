# Azuma's env setting repo
#### some config file and setup script i always used when reinstall my ubuntu system

## OS
1. #### install ubuntu 18.09 gnome3 deskop -> minimal
2. #### install aptitude git with apt
3. #### `git clone https://github.com/0711kps/dofile`
- > ##### with "light", system take about 37G to install, recommend 37 * 110% + 5G ~= 46G
- > ##### with "full", system take about 51G to install, recommend 51 * 110% + 5G ~= 61G
## SSH
* #### append public key to ~/.ssh/authorized_keys file to allow someone's remote ssh access
## locale change, e.g.
1. #### locale-gen en_US.UTF-8
2. #### dpkg-reconfigure locales
3. #### update-locale LANG=en_US.UTF-8
## gnome-tweaks
### Appearance
- #### Themes
  - ##### Applications
    - Arc-Dark
  - ##### Cursor
    - Sugar
  - ##### Icons
  -   Yaru
  - ##### Sound
    - Yaru
- #### fonts
  - ##### Interface Text
    - Noto Sans CJK TC Regular, 13px
  - ##### Document Text
    - Noto Sans CJK TC Regular, 13px
  - ##### Monospace Text
    - Ricty Diminished Regular, 16px
  - ##### Legacy Window Titles
    - Noto Sans CJK TC Bold, 13px
  - ##### Hinting
    - None
  - ##### Antialiasing
    - Subpixel
- #### TopBar:
  - ##### Clock
    - ##### enable Weekday, Date, and Seconds
  - ##### Calendar
    - ##### enable Week Numbers
  - ##### WindowTitlebar
    - ##### TitlebarButtons-Placement: Left
    - ##### Maximize: off
    - ##### Minimize: off
## git-server e.g.
#### server(openssh-server and git installed)
1. ##### mkdir example-project
2. ##### cd example-project
3. ##### git init --bare (a server-side project should use --bare flag)
#### client
1. ##### git clone user@host:/path/to/project-dir(e.g. git@mygitserver.com:~/code/example-project)
2. ##### (make some change...)
3. ##### git add -A
4. ##### git commit -m 'ooxx'
5. ##### git push
## check port usage
- #### lsof -i
- #### lsof -i | rg TCP
- #### lsof -i:80 | rg listen
## rsync
- ### server
  - #### sudo rsync --daemon
- ### client
  - #### from remote to local
    - rsync -chavzP --stats user@remote.host:/path/to/copy /path/to/local/storage
* neovim compile *
git clone https://github.com/neovim/neovim
sudo make CMAKE_BUILD_TYPE=Release CMAKE_EXTRA_FLAGS="-DCMAKE_INSTALL_PREFIX=/opt/neovim"
sudo make -j4
sudo make install
**** emacs compile *
git clone https://github.com/emacs-mirror/emacs
./autogen.sh
./configure --without-all --without-x --enable-link-time-optimization --prefix=/opt/emacs
sudo make -j4
sudo make install
