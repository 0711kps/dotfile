if [[ $EUID -eq 0 ]]; then
  echo "don't run this script with root" 1>&2
  exit 1
else
  rm -f log/*.log
  source lib/parseOpts.sh
  parseOpts $@
  source lib/color.sh
  if [ -z "$ENV" ]; then
    failText "environment not assign!"
    echo ''
    parseOpts -h
  fi
  if [[ $ENV != 'full' ]]  && [[ $ENV != 'light' ]]; then
    noticeText "unknown environment $(failText "$ENV")"
    echo ''
    parseOpts -h
  fi
  source lib/prepareTools.sh
  source lib/app.sh
  prepareTools
  aptApps
  brewApps
  snapApps
  for i in $que
  do
    wait $i
  done
  unset que
  otherApps
  nodeApps
  gemApps
  for i in $que
  do
    wait $i
  done
  unset que
  removeApps
  afterInstall
  for i in $que
  do
    wait $i
  done
  afterHint
fi
